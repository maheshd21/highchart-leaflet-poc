import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { HighchartService } from '../highchart.service';
import { AgmMap, LatLngBounds } from '@agm/core';
declare let L :any; 
import { icon, Map, latLng, marker, polyline, tileLayer } from 'leaflet';

@Component({
  selector: 'app-map-test',
  templateUrl: './map-test.component.html',
  styleUrls: ['./map-test.component.scss']
})
export class MapTestComponent implements OnInit {
  @ViewChild('techniciansChart', null) public techniciansChart: ElementRef;
  @ViewChild('AgmMap', null) agmMap: AgmMap;
  
  title = 'My first AGM project';
  lat = 51.678418;
  lng = 7.809007;
  map: Map;
  //initial center position
  // lat: Number = 24.799448;
  // lng: Number = 120.979021;
  //google maps zoom
  zoom: Number = 16;
  lines = [];
  trackings = [];
  // Define our base layers so we can reference them multiple times
  streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });
  wMaps = tileLayer('https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });

  // Marker for the top of Mt. Ranier
  summit = marker([ 46.8523, -121.7603 ], {
    icon: icon({
      iconSize: [ 25, 41 ],
      iconAnchor: [ 13, 41 ],
      iconUrl: 'leaflet/marker-icon.png',
      shadowUrl: 'leaflet/marker-shadow.png'
    })
  });

  // Marker for the parking lot at the base of Mt. Ranier trails
  paradise = marker([ 46.78465227596462,-121.74141269177198 ], {
    icon: icon({
      iconSize: [ 25, 41 ],
      iconAnchor: [ 13, 41 ],
      iconUrl: 'leaflet/marker-icon.png',
      shadowUrl: 'leaflet/marker-shadow.png'
    })
  });

  // Path from paradise to summit - most points omitted from this example for brevity
  route = polyline([
    [ 46.78465227596462,-121.74141269177198 ],
    [ 46.80047278292477, -121.73470708541572 ],
    [ 46.815471360459924, -121.72521826811135 ],
    [ 46.8360239546746, -121.7323131300509 ],
    [ 46.844306448474526, -121.73327445052564 ],
    [ 46.84979408048093, -121.74325201660395 ],
    [ 46.853193528950214, -121.74823296256363 ],
    [ 46.85322881676257, -121.74843915738165 ],
    [ 46.85119913890958, -121.7519719619304 ],
    [ 46.85103829018772, -121.7542376741767 ],
    [ 46.85101557523012, -121.75431755371392 ],
    [ 46.85140013694763, -121.75727385096252 ],
    [ 46.8525277543813, -121.75995212048292 ],
    [ 46.85290292836726, -121.76049157977104 ],
    [ 46.8528160918504, -121.76042997278273 ]]);

  // Layers control object with our two base layers and the three overlay layers
  layersControl:any = {
    baseLayers: {
      'Street Maps': this.streetMaps,
      'Wikimedia Maps': this.wMaps
    },
    overlays: {
      // 'Mt. Rainier Summit': this.summit,
      // 'Mt. Rainier Paradise Start': this.paradise,
      'Mt. Rainier Climb Route': this.route
    }
  };


  // Set the initial set of displayed layers (we could also use the leafletLayers input binding for this)
  options = {
    layers: [ this.streetMaps, this.route, this.summit, this.paradise ],
    zoom: 12,
    center: latLng([  42.322354, -71.08087 ])
  };
  isEnabled: boolean = true;
  constructor(private highcharts: HighchartService,
    private changeDetector: ChangeDetectorRef) {
    console.log(this.route)
   }

  ngAfterViewInit() {
    console.log(this.agmMap);
  }

  onMapReady(map: Map) {
    // Do stuff with map
    this.map = map;
 }
  ngOnInit() {
    this.trackings = [{ 
      id: 1,
      "name": "State A",
      username: "Johny",
      "data": [{
        //refers to User 1
        "x": 1540168200000, 
        "x2": 1540186200000,
        "y": 0,
        geotracks: [{
          latitude: 42.344308,
          longitude: -71.068515
        }, {
            latitude: 42.335933,
            longitude: -71.078811
        }]
    }, {
      //refers to User 2
      "x": 1540168200000,
      "x2": 1540204200000,
      "y": 1,
      geotracks: [{
        latitude: 42.335933,
        longitude: -71.078811
      }, {
          latitude: 42.333903,
          longitude: -71.099401
      }]
    }, {
      //refers to User 3
      "x": 1540173600000,
      "x2": 1540182600000,
      "y": 2,
      geotracks: [{
        latitude: 42.333903,
        longitude: -71.099401
      }, {
          latitude: 42.327050,
          longitude: -71.092709
      }]
    },
    {
      //refers to User 1
      "x": 1540211400000, 
      "x2": 1540218600000,
      "y": 0,
      geotracks: [{
        latitude: 42.327050,
        longitude: -71.092709
      }, {
          latitude: 42.322354,
          longitude: -71.080870
      }]
  }
  ],
  dataLabels: {
    enabled: true
},
    pointWidth: 20
  }, {
      id: 2,
      username: "Mike",
      "name": "State B",
      "data": [{
        //refers to User 1
        "x": 1540186200000,
        "x2": 1540200600000,
        "y": 0,
        geotracks: [{
          latitude: 42.322354,
          longitude: -71.080870
        }, {
            latitude: 42.335679,
            longitude: -71.105750
        }]
      }, {
        //refers to User 2
        "x": 1540204200000,
        "x2": 1540211400000,
        "y": 1,
        geotracks: [{
          latitude: 42.335679,
          longitude: -71.105750
        }, {
            latitude: 42.334537,
            longitude: -71.12146
        }]
      }, {
        //refers to User 3
        "x": 1540182600000,
        "x2": 1540193400000,
        "y": 2,
        geotracks: [{
          latitude: 42.334537,
            longitude: -71.12146
        }, {
            latitude: 42.326162,
            longitude: -71.124035
        }]
      },
      {
        //refers to User 2
        "x": 1540218600000, 
        "x2": 1540225800000,
        "y": 1,
        geotracks: [{
          latitude: 42.326162,
          longitude: -71.124035
        }, {
            latitude: 42.313088,
            longitude: -71.1305
        }]
    }
    ],
      pointWidth: 20
      
  }, {
    id: 3,
    username: "Adam",
    "name": "State C",
    "data": [{
      //refers to User 1
      "x": 1540200600000,
      "x2": 1540211400000,
      "y": 0,
      geotracks: [{
        latitude: 42.313088,
        longitude: -71.1305
      }, {
          latitude: 42.305979,
          longitude: -71.133304
      }]
    }, {
      //refers to User 2
      "x": 1540211400000,
      "x2": 1540218600000,
      "y": 1,
      geotracks: [{
        latitude: 42.305979,
          longitude: -71.133304
      }, {
          latitude: 42.304837,
          longitude: -71.154752
      }]
    }, {
      //refers to User 3
      "x": 1540193400000,
      "x2": 1540207800000,
      "y": 2,
      geotracks: [{
        latitude: 42.304837,
        longitude: -71.154752
      }, {
          latitude: 42.292521,
          longitude: -71.15060
      }]
    },
    {
      //refers to User 2
      "x": 1540218600000, 
      "x2": 1540225800000,
      "y": 2,
      geotracks: [{
        latitude: 42.326162,
        longitude: -71.124035
      }, {
          latitude: 42.313088,
          longitude: -71.1305
      }]
  }
  ],
    pointWidth: 20
  }];


  this.initStackedBarTechicians();

  }

  setTechnician(id){
    let techId = id;
    this.lines = this.trackings.find(a => a.id === techId ).geotracks;
    this.lat = this.lines[0].latitude;
    this.lng = this.lines[0].longitude;
    console.log(this.lines);
  }
  setByGeotracks(point){
    let geotracks = point.geotracks;
    let name = point.yCategory;
    this.lines = geotracks;
    this.lat = geotracks[0].latitude;
    this.lng = geotracks[0].longitude;

    this.route = polyline(this.lines.map(a => [a.latitude, a.longitude]));
    console.log(this.route)
    this.options = {
      layers: [ this.streetMaps, this.route, this.summit, this.paradise ],
      zoom: 7,
      center: latLng(this.lines[0])
    };
    this.drawMarkerAndLines(name);
  }
  setTechnicianByName(name){
    this.lines = []; 
    this.trackings.find(a => a.username === name ).data.map(a => a.geotracks).forEach((a:any) => {
      this.lines.push(...a);
    });
    console.log(this.lines, name);
    //.geotracks;
    this.lat = this.lines[0].latitude;
    this.lng = this.lines[0].longitude;
    console.log(this.lines);
    this.drawMarkerAndLines(name);
  }

  drawMarkerAndLines(name){
    let m:any = this.map;
    for(let i in m._layers) {
      if(m._layers[i]) {
          try {
              m.removeLayer(m._layers[i]);
          }
          catch(e) {
              console.log("problem with " + e + m._layers[i]);
          }
      }
  }
    let newroute:any = polyline(this.lines.map(a => {
      L.marker([a.latitude, a.longitude], { title: "My marker" }).addTo(this.map);
      return [a.latitude, a.longitude]
    }));
    console.log(this.route, this.map, this.layersControl)
   
    this.layersControl.overlays[name] = newroute;
    
    newroute.addTo(this.map);
    var lat = this.lines.map(c => c.latitude).reduce((previous, current) => current += previous, 0.0);
    console.log(lat)
    var long = this.lines.map(c => c.longitude).reduce((previous, current) => current += previous, 0.0);
    var positions:any = [lat / this.lines.length, long / this.lines.length];
    this.map.setView(positions, 13);
  }

  initStackedBarTechicians(){
    let self = this;
    let groups = [1, 1, 1];
    let tickPositions = [-1];
    for (let i = 0; i < groups.length; i++) {
      tickPositions.push(tickPositions[i] + groups[i]);
    }
    let options = {
     
      chart: {
        type: 'xrange',
        // backgroundColor: '#22343c',
        plotBorderColor: '#606063',
        events: {
          dblclick: function (a) {
              console.log('dbl ',a)
          },
          click: function (a) {
          },
          contextmenu: function (a) {
            console.log('sasa ds',a)
          }
  },
    },
    title: {
        text: 'Technicians details'
    },
    xAxis: {
        type: 'datetime',
        labels: {
          events: {
            click: function() {
              console.log(this)
            }
          }
        }
    },
    yAxis: {
        title: {
            text: 'Technician timeline'
        },
        categories: self.trackings.map(a => a.username),
        tickPositions: tickPositions,
        lineWidth: 0,
        labels: {
      formatter: function() {
        var chart = options.chart,
          axis = this.axis,
          label;

        //@ts-ignore
        if (!chart.yaxisLabelIndex) {
        //@ts-ignore
        chart.yaxisLabelIndex = 0;
        }
        console.log(this.value);
        if (this.value !== -1) {

          //@ts-ignore
          label = axis.categories[chart.yaxisLabelIndex];
        //@ts-ignore
        chart.yaxisLabelIndex++;

        axis.labelGroup.element.childNodes.forEach(function(lbl)
        {
          lbl.style.cursor = "pointer";
          lbl.onclick = function(){
            self.setTechnicianByName(this.textContent);
              console.log('You clicked on '+this.textContent);
            }
        });


          //@ts-ignore
        if (chart.yaxisLabelIndex === groups.length) {
            //@ts-ignore
        chart.yaxisLabelIndex = 0;
          }

          return label;
        }
      },
    },
    
        reversed: true
    },
    legend: {
        enabled: true
    },
    plotOptions: {
        series: {
            // stacking: 'stacked',
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y}'
            },
            //"grouping": false,
            cursor: 'pointer',
          point: {
              events: {
                  click: function (e:any) {
                      console.log(e);
                      self.setByGeotracks(e.point);
                  }
              }
          }
        },
        "xrange": {
          "borderRadius": 0,
          "borderWidth": 0,
          "grouping": false,
          "dataLabels": {
            "align": "center",
            "enabled": true,
            "format": "{point.name}"
          },
          "colorByPoint": false
        }
    },
    // hour: '%H:%M',
    // day: '%e. %b',
    // week: '%e. %b',
    // month: '%b \'%y',
    // year: '%Y'
    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}"></span> <b>{point.x:%e %b %Y %H:%M} - {point.x2:%e %b %Y %H:%M}</b> stops<br/>'
    },
series: this.trackings
//     series: [
//         {
//             name: "Idle",
//             color: 'red',
//             data: self.trackings
//         },
//         {
//           name: "Working",
//           color: 'blue',
//           data: self.trackings
//       },
//       {
//         name: "Travelling",
//         color: 'orange',
//         data: self.trackings
//     },
//     {
//       name: "Working",
//       color: 'blue',
//       data: self.trackings
//   },
//   {
//     name: "Idle",
//     color: 'red',
//     data: self.trackings
// },
//     ]
    };

    this.highcharts.createChart(this.techniciansChart.nativeElement, options);
  }
}

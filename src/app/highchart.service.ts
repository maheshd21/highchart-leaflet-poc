import { Injectable } from '@angular/core';
import * as Highcharts from 'highcharts';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import Exporting from 'highcharts/modules/exporting';

// Initialize exporting module.
Exporting(Highcharts);

@Injectable({
  providedIn: 'root'
})
export class HighchartService {

  token = 'Bearer ya29.ImCxB7IUUY8RszMXtW76xWcRvwlMjW5HHrkGvCYdHrkQdXtwaKJpWKRNfyNJK1POFrpIJXVGYO1E_Ak9cx-xorIckmTrlU62EAnxf1nKvWmQ1Rk-7M6jCySqYNZhehSEmP8';

  constructor(private http: HttpClient) { }
  frmTo = `from=1573549578&to=1573553266`;
  createChart(el, cfg) {
    Highcharts.chart(el, cfg);
  }

  getDatadogData() {
    return this.http.get(`https://api.datadoghq.com/api/v1/query?${this.frmTo}&query=per_minute(sum:trace.express.request.hits{env:local,service:maleficent}.as_count())`);
  }

  getDiskUsageData() {
    return this.http.get(`https://api.datadoghq.com/api/v1/query?${this.frmTo}&query=avg:system.disk.total{host:BEK-98783412,device:c:},avg:system.disk.used{host:BEK-98783412,device:c:}`);
  }
  get404Count(){
    return this.http.get(`https://api.datadoghq.com/api/v1/query?${this.frmTo}&query=avg:trace.express.request.hits.by_http_status{env:local,host:BEK-98783412,http.status_class:4xx,http.status_code:404,service:maleficent}.as_count()`);
  }

  getMysqlCount(){
    return this.http.get(`https://api.datadoghq.com/api/v1/query?${this.frmTo}&query=avg:trace.mysql.query.hits{env:local}.as_count()`);
  }

  getActiveUserAndPageViewsFromGoogle(){
    let headers = new HttpHeaders({
      'authorization': this.token });
    let options = { headers: headers };
    let data = {
      "reportRequests": [
        {
          "viewId": "206028480",
          "dateRanges": [
            {
              "startDate": "30daysAgo",
              "endDate": "2019-11-20"
            }
          ],
          "metrics": [
            {
              "expression": "ga:users"
            },
            {
              "expression": "ga:pageviews"
            }
          ],
          "dimensions": [
            {
              "name": "ga:day",
              "histogramBuckets": [
                "1"
              ]
            },
            {
              "name": "ga:segment"
            }
          ],
          "segments": [
            {
              "segmentId": "gaid::-1"
            }
          ],
          "orderBys": [
            {
              "fieldName": "ga:day",
              "orderType": "HISTOGRAM_BUCKET",
              "sortOrder": "ASCENDING"
            }
          ]
        }
      ]
    };
    return this.http.post(`https://content-analyticsreporting.googleapis.com/v4/reports:batchGet?alt=json`, data,  options );
  }

  getTopPages(){
    let headers = new HttpHeaders({
      'authorization': this.token });
    let options = { headers: headers };
    let data = {"reportRequests":[{"viewId":"206028480","dateRanges":[{"startDate":"30daysAgo","endDate":"2019-11-20"}],"metrics":[{"expression":"ga:pageviews"}],"dimensions":[{"name":"ga:pagePath"},{"name":"ga:segment"}],"segments":[{"segmentId":"gaid::-1"}],"pivots":[{"dimensions":[{"name":"ga:userType"}],"metrics":[{"expression":"ga:users"}]}]}]};

    return this.http.post(`https://content-analyticsreporting.googleapis.com/v4/reports:batchGet?alt=json`, data,  options );
  }

}
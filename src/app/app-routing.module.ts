import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BcgDashboardComponent } from './bcg-dashboard/bcg-dashboard.component';
import { ConfigureAppsComponent } from './configure-apps/configure-apps.component';
import { MapTestComponent } from './map-test/map-test.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


const routes: Routes = [
  {
    path: 'dashboard',
    component: BcgDashboardComponent
  },
  {
    path: 'map',
    component: MapTestComponent
  },
  {
    path: '',
    pathMatch: 'full',
    //component: ConfigureAppsComponent
    redirectTo: '/map'
  },
  {path: '404', component: PageNotFoundComponent},
  {path: '**', redirectTo: '/404'}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

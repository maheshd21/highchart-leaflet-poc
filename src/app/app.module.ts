import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChartModule, HIGHCHARTS_MODULES } from 'angular-highcharts';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AddHeaderInterceptor } from './req-interceptor';
import { BcgDashboardComponent } from './bcg-dashboard/bcg-dashboard.component';
import * as highcharts3d from 'highcharts/highcharts-3d';
import { ConfigureAppsComponent } from './configure-apps/configure-apps.component';

import * as HighchartsMore from 'highcharts/highcharts-more';
import * as HighchartsSolidGauge from 'highcharts/modules/solid-gauge';
import * as xrange from "highcharts/modules/xrange";
import { AgmCoreModule } from '@agm/core';
import { MapTestComponent } from './map-test/map-test.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@NgModule({
  declarations: [
    AppComponent,
    BcgDashboardComponent,
    ConfigureAppsComponent,
    MapTestComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ChartModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAepS0jcNBZ66emBOGVhlpsW2dP3cqoxxg'
    }),
    LeafletModule.forRoot()
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AddHeaderInterceptor,
    multi: true,
  },
  { provide: HIGHCHARTS_MODULES, useFactory: () => [ highcharts3d, HighchartsMore, HighchartsSolidGauge, xrange ] }
],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BcgDashboardComponent } from './bcg-dashboard.component';

describe('BcgDashboardComponent', () => {
  let component: BcgDashboardComponent;
  let fixture: ComponentFixture<BcgDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BcgDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BcgDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

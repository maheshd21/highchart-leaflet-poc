import {  Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { Chart } from 'angular-highcharts';
import { HighchartService } from '../highchart.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-bcg-dashboard',
  templateUrl: './bcg-dashboard.component.html',
  styleUrls: ['./bcg-dashboard.component.scss']
})
export class BcgDashboardComponent implements OnInit {
  @ViewChild('chart404', null) public chart404: ElementRef;
  @ViewChild('chartLatency', null) public chartLatency: ElementRef;
  @ViewChild('dataDogChart', null) public dataDogChart: ElementRef;
  @ViewChild('diskUsageChart', null) public diskUsageChart: ElementRef;
  @ViewChild('chartUsers', null) public chartUsers: ElementRef;
  @ViewChild('pageviews', null) public pageviews: ElementRef;
  
  title = 'bcg-dashboard';
  ddData: any;
  diskUsage: any;
  total404Res: any;
  countOf404: any;
  totalMysqlRes: any;
  totalMysqlOptions: any;
  constructor(private highcharts: HighchartService) { }
  ngOnInit(){
    // this.highcharts.createChart(this.chartEl.nativeElement, this.countOf404);
    // this.highcharts.createChart(this.solarChart.nativeElement, this.solarOptions);
    
    this.highcharts.getDatadogData().subscribe(res => {
      this.ddData = res;
     let dataDogChartOptions:any = {
        chart: { type: 'area', height: 250,
        
      },
        title: { text: 'Total API requests count', },
        xAxis: { type: 'datetime' },
    
        series: [
          {
            // type: 'area',
            name: 'Total API requests count',
            data: this.ddData.series[0].pointlist,
            // color: 'black',
            tooltip: {valueSuffix: ' Per Min'}
          },
          // {
          //   name: 'Humidity',
          //   data: [
          //     [1373228000, 58],
          //     [1373928000, 76],
          //     [1374228000, 26],
          //   ],
          // }
        ]
      }
      
      this.highcharts.createChart(this.dataDogChart.nativeElement, dataDogChartOptions);

    });

    this.highcharts.getDiskUsageData().subscribe(res => {
        this.diskUsage = res;

        let arrDisk = [];
        this.diskUsage.series.forEach((element,i) => {
          //if (i === 0) {
          let el = element.pointlist.map(a => a[1]);
          let sum = el.reduce(function (a, b) { return a + b; });
          let avg = sum / el.length;
          let avgCount = parseInt((avg/1048/1048/1048).toFixed(3));
          arrDisk.push({
            avgCount
          });
          //}
        });
        console.log(arrDisk);
        let dataDogChartOptions:any = {
          chart: {
            type: 'solidgauge'
        },
        title: { text: 'Disk utilization ' },
        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor:
                    '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },
    
        exporting: {
            enabled: false
        },
    
        tooltip: {
            enabled: false
        },
    
        // the value axis
        yAxis: {
          min: 0,
          max: arrDisk[0].avgCount,
          title: {
              text: 'Disk'
          },
            stops: [
                [0.1, '#55BF3B'], // green
                [0.5, '#DDDF0D'], // yellow
                [0.9, '#dc3545'] // red
            ],
            lineWidth: 0,
            tickWidth: 0,
            minorTickInterval: null,
            tickAmount: 2,
            labels: {
                y: 16
            }
        },
    
        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        },

      credits: {
          enabled: false
      },

      series: [{
          name: 'Speed',
          data: [arrDisk[1].avgCount],
          dataLabels: {
              format:
                  '<div style="text-align:center">' +
                  '<span style="font-size:25px">{y}</span><br/>' +
                  '<span style="font-size:12px;opacity:0.4">GB</span>' +
                  '</div>'
          },
          tooltip: {
              valueSuffix: ' GB'
          }
      }]
        };
        
        this.highcharts.createChart(this.diskUsageChart.nativeElement, dataDogChartOptions);
  
    });

    this.highcharts.get404Count().subscribe(res => {
      this.total404Res = res;

      this.countOf404 = {
        chart: { type: 'column', height: 250,
          renderTo: 'container',
        //   options3d: {
        //     enabled: true,
        //     alpha: 15,
        //     beta: 15,
        //     depth: 50,
        //     viewDistance: 25
        // }
        },
          title: { text: '404 Error count on app' },
          xAxis: { type: 'datetime',
          labels: {
            //format: '{value:%d.%m.%Y %A %s} ',
            rotation: 0,
            align: 'left'
          }
        },
          
          // plotOptions: {
          //   series: {
          //       stacking: 'normal'
          //   }
          // },
          plotOptions: {
            column: {
              depth: 25
          }
            //series: {
              //       stacking: 'normal'
              //   }
        },
          series: []
        };

      this.total404Res.series.forEach((element,i) => {
        //if (i === 0) {
          this.countOf404.series.push({
            // type: 'area',
            name: 'Error count',
            data: element.pointlist.splice(0,14),
            // color: 'black',
            tooltip: { valueSuffix: ' Per Min' }
          });
        //}
      });

      
      this.highcharts.createChart(this.chart404.nativeElement, this.countOf404);
    });

    this.highcharts.getMysqlCount().subscribe(res => {
      this.totalMysqlRes = res;

      this.totalMysqlOptions = {
        chart: { type: 'area', height: 250,
        },
          title: { text: 'Mysql Query Count' },
          xAxis: { type: 'datetime',
          labels: {
            //format: '{value:%d.%m.%Y %A %s} ',
            rotation: 0,
            align: 'left'
          }
        },
          
          // plotOptions: {
          //   series: {
          //       stacking: 'normal'
          //   }
          // },
          plotOptions: {
            column: {
              depth: 25
          }
            //series: {
              //       stacking: 'normal'
              //   }
        },
          series: []
        };

      this.totalMysqlRes.series.forEach((element,i) => {
        //if (i === 0) {
          this.totalMysqlOptions.series.push({
            // type: 'area',
            name: element.metric,
            data: element.pointlist.splice(0,14),
            // color: 'black',
            tooltip: { valueSuffix: ' Per Min' }
          });
        //}
      });

      
      // this.highcharts.createChart(this.chartLatency.nativeElement, this.totalMysqlOptions);
    });

    this.highcharts.getActiveUserAndPageViewsFromGoogle().subscribe((res:any) => {
      console.log(res);
      let gData = [];

      res.reports[0].columnHeader.metricHeader.metricHeaderEntries.forEach(element => {
        element.name = element.name.slice(3);
        element.y = element.value;
        gData.push(element);
      });

      res.reports[0].data.totals[0].values.forEach((element, i)=> {
        gData[i].y = parseInt(element);
        gData[i].value = element;
      });
      console.log(gData);

      let usersOptions = {
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
      },
      title: {
          text: 'Active users from last 30 days along with page views'
      },
      // tooltip: {
      //     pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      // },
      plotOptions: {  
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  // format: '<b>{point.name}</b>: {point.percentage:.1f} %'
              }
          }
      },
      series: [{
          name: 'value',
          colorByPoint: true,
          data: gData
      }]
      };

      this.highcharts.createChart(this.chartUsers.nativeElement, usersOptions);
      
    });

    this.highcharts.getTopPages().subscribe((res:any) => {
      console.log(res);
      
      let pagesArr = [];
      var newList = [];
      

      res.reports[0].data.rows.forEach(element => {
          let page = element.dimensions[0];
          let views = parseInt(element.metrics[0].values[0]);
          if (page == '/') {
            page = '/homepage'
          }
          pagesArr.push({
            page: page,
            views: views,
            name: page,
            y: views,
            data: [views]
          })
      });
      pagesArr = pagesArr.sort(function (a, b) {
        return b.views - a.views
      });
      pagesArr.forEach(a => {
        var d = newList.filter(r => r.page == a.page).length;
        if ( d == 0 && !a.page.includes('story') && !a.page.includes("dashoard")) {
          newList.push(a);
        }
      });
      pagesArr = newList;
      // pagesArr = pagesArr.filter(a => !a.page.includes('story'));
      console.log(pagesArr);

      let columnOpt = {
        chart: {
          type: 'bar'
      },
      title: {
          text: 'Most visted pages'
      },
      // subtitle: {
      //     text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
      // },
      xAxis: {
          type: 'category'
      },
      yAxis: {
          title: {
              text: 'Most visted pages'
          }
  
      },
      legend: {
          enabled: false
      },
      plotOptions: {
          series: {
              borderWidth: 0,
              dataLabels: {
                  enabled: true,
              }
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
          pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
      },
  
      series: [
          {
              name: "Page Views",
              colorByPoint: true,
              data: pagesArr
          }
      ]
      }
      this.highcharts.createChart(this.pageviews.nativeElement, columnOpt);
    })
  };

  

  
  
}

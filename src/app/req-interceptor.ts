import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
  } from '@angular/common/http';
  import { Observable } from 'rxjs';
  
  export class AddHeaderInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      // Clone the request to add the new header
      let clonedRequest = req.clone({ setHeaders : {'DD-API-KEY' : 'f1aed7c9bda0b2ba6785f490deda0bee',
      'DD-APPLICATION-KEY' : '9876f5c3c9c85db99d4f3836e7839f08dca5abcf'} });
  console.log(clonedRequest)
  
      // Pass the cloned request instead of the original request to the next handle
      return next.handle(clonedRequest);
    }
  }